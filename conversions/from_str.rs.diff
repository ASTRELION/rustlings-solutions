@@ -1,26 +1,25 @@
 // from_str.rs
-//
-// This is similar to from_into.rs, but this time we'll implement `FromStr` and
-// return errors instead of falling back to a default value. Additionally, upon
-// implementing FromStr, you can use the `parse` method on strings to generate
-// an object of the implementor type. You can read more about it at
-// https://doc.rust-lang.org/std/str/trait.FromStr.html
-//
-// Execute `rustlings hint from_str` or use the `hint` watch subcommand for a
-// hint.
+// This is similar to from_into.rs, but this time we'll implement `FromStr`
+// and return errors instead of falling back to a default value.
+// Additionally, upon implementing FromStr, you can use the `parse` method
+// on strings to generate an object of the implementor type.
+// You can read more about it at https://doc.rust-lang.org/std/str/trait.FromStr.html
+// Execute `rustlings hint from_str` or use the `hint` watch subcommand for a hint.
 
 use std::num::ParseIntError;
 use std::str::FromStr;
 
 #[derive(Debug, PartialEq)]
-struct Person {
+struct Person
+{
     name: String,
     age: usize,
 }
 
 // We will use this error type for the `FromStr` implementation.
 #[derive(Debug, PartialEq)]
-enum ParsePersonError {
+enum ParsePersonError
+{
     // Empty input string
     Empty,
     // Incorrect number of fields
@@ -31,53 +30,83 @@
     ParseInt(ParseIntError),
 }
 
-// I AM NOT DONE
-
 // Steps:
 // 1. If the length of the provided string is 0, an error should be returned
 // 2. Split the given string on the commas present in it
-// 3. Only 2 elements should be returned from the split, otherwise return an
-//    error
+// 3. Only 2 elements should be returned from the split, otherwise return an error
 // 4. Extract the first element from the split operation and use it as the name
-// 5. Extract the other element from the split operation and parse it into a
-//    `usize` as the age with something like `"4".parse::<usize>()`
-// 6. If while extracting the name and the age something goes wrong, an error
-//    should be returned
+// 5. Extract the other element from the split operation and parse it into a `usize` as the age
+//    with something like `"4".parse::<usize>()`
+// 6. If while extracting the name and the age something goes wrong, an error should be returned
 // If everything goes well, then return a Result of a Person object
 //
-// As an aside: `Box<dyn Error>` implements `From<&'_ str>`. This means that if
-// you want to return a string error message, you can do so via just using
-// return `Err("my error message".into())`.
+// As an aside: `Box<dyn Error>` implements `From<&'_ str>`. This means that if you want to return a
+// string error message, you can do so via just using return `Err("my error message".into())`.
 
-impl FromStr for Person {
+impl FromStr for Person
+{
     type Err = ParsePersonError;
-    fn from_str(s: &str) -> Result<Person, Self::Err> {
+    fn from_str(s: &str) -> Result<Person, Self::Err>
+    {
+        if s.len() == 0
+        {
+            return Err(ParsePersonError::Empty);
+        }
+        let split: Vec<&str> = s.split(",").collect();
+        if split.len() != 2
+        {
+            return Err(ParsePersonError::BadLen);
+        }
+        let name = split[0];
+        if name.len() == 0
+        {
+            return Err(ParsePersonError::NoName)
+        }
+        let age_result = split[1].parse::<usize>();
+        match age_result
+        {
+            Ok(a) =>
+            {
+                Ok(Person { name: name.into(), age: a })
+            }
+            Err(e) =>
+            {
+                Err(ParsePersonError::ParseInt(e))
+            }
+        }
     }
 }
 
-fn main() {
+fn main()
+{
     let p = "Mark,20".parse::<Person>().unwrap();
     println!("{:?}", p);
 }
 
 #[cfg(test)]
-mod tests {
+mod tests
+{
     use super::*;
 
     #[test]
-    fn empty_input() {
+    fn empty_input()
+    {
         assert_eq!("".parse::<Person>(), Err(ParsePersonError::Empty));
     }
+
     #[test]
-    fn good_input() {
+    fn good_input()
+    {
         let p = "John,32".parse::<Person>();
         assert!(p.is_ok());
         let p = p.unwrap();
         assert_eq!(p.name, "John");
         assert_eq!(p.age, 32);
     }
+
     #[test]
-    fn missing_age() {
+    fn missing_age()
+    {
         assert!(matches!(
             "John,".parse::<Person>(),
             Err(ParsePersonError::ParseInt(_))
@@ -85,7 +114,8 @@
     }
 
     #[test]
-    fn invalid_age() {
+    fn invalid_age()
+    {
         assert!(matches!(
             "John,twenty".parse::<Person>(),
             Err(ParsePersonError::ParseInt(_))
@@ -93,17 +123,20 @@
     }
 
     #[test]
-    fn missing_comma_and_age() {
+    fn missing_comma_and_age()
+    {
         assert_eq!("John".parse::<Person>(), Err(ParsePersonError::BadLen));
     }
 
     #[test]
-    fn missing_name() {
+    fn missing_name()
+    {
         assert_eq!(",1".parse::<Person>(), Err(ParsePersonError::NoName));
     }
 
     #[test]
-    fn missing_name_and_age() {
+    fn missing_name_and_age()
+    {
         assert!(matches!(
             ",".parse::<Person>(),
             Err(ParsePersonError::NoName | ParsePersonError::ParseInt(_))
@@ -111,7 +144,8 @@
     }
 
     #[test]
-    fn missing_name_and_invalid_age() {
+    fn missing_name_and_invalid_age()
+    {
         assert!(matches!(
             ",one".parse::<Person>(),
             Err(ParsePersonError::NoName | ParsePersonError::ParseInt(_))
@@ -119,12 +153,14 @@
     }
 
     #[test]
-    fn trailing_comma() {
+    fn trailing_comma()
+    {
         assert_eq!("John,32,".parse::<Person>(), Err(ParsePersonError::BadLen));
     }
 
     #[test]
-    fn trailing_comma_and_some_string() {
+    fn trailing_comma_and_some_string()
+    {
         assert_eq!(
             "John,32,man".parse::<Person>(),
             Err(ParsePersonError::BadLen)
