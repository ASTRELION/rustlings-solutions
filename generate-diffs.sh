#!/bin/bash

if [[ ! -d "$1" ]]; then
    echo "Invalid Rustlings directory."
    exit 1
fi


generate_diff()
{
    dir1="$1"
    dir2="$2"
    file="$3"

    diff -u "$dir2/$file" "$dir1/$file" | tail -n +3 > "$dir1/$file.diff"
}

# dir1 will be where the actual diffs are generated
# dir1 should be the directory with the finished solutions
dir1="$PWD"
# dir2 should be the unmodified directory of the rustlings exercises
dir2="$1"

find "$dir1" -type f -name "*.rs" | while read file; do
    path="${file#$dir1/}"
    generate_diff "$dir1" "$dir2" "$path"
    echo "Generated diff for $path"
done
